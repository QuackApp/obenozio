﻿using Assets.Scripts.Data;
using Assets.Scripts.Utils;
using UnityEngine;

public class AppManager : BaseSingleton<AppManager>
{
    #region Members
    
    [Header("Managers")]
    [SerializeField]
    private Transform _managersContainer;

    [SerializeField]
    private UILoaderManager _uiLoaderManager;

    [SerializeField]
    private ChatLobbyManager _chatLobbyManagerPFB;    

    [SerializeField]
    private PopupManager _popupManagerPFB;

    [SerializeField]
    private RegistrationManager _registrationManagerPFB;

    [SerializeField]
    private SceneSelectionManager _sceneSelectionManagerPFB;    

    [SerializeField]
    private SceneCharacterSelectionManager _characterSelectionManagerPFB;

    [SerializeField]
    private ChatRoomManager _chatRoomManagerPFB;

    #endregion // Members

    #region Private Fields

    private ChatLobbyManager _chatLobbyManager;
    private PopupManager _popupManager;
    private RegistrationManager _registrationManager;
    private SceneSelectionManager _sceneSelectionManager;
    private SceneCharacterSelectionManager _characterSelectionManager;
    private ChatRoomManager _chatRoomManager;

    #endregion

    #region Props

    public ChatLobbyManager ChatLobbyManager
    {
        get
        {
            return _chatLobbyManager;
        }
    }

    public PopupManager PopupManager
    {
        get
        {
            return _popupManager;
        }
    }

    public RegistrationManager RegistrationManager
    {
        get
        {
            return _registrationManager;
        }
    }

    public SceneCharacterSelectionManager CharacterSelectionManager
    {
        get
        {
            return _characterSelectionManager;
        }

        set
        {
            _characterSelectionManager = value;
        }
    }

    public SceneSelectionManager SceneSelectionManager
    {
        get
        {
            return _sceneSelectionManager;
        }

        set
        {
            _sceneSelectionManager = value;
        }
    }

    public ChatRoomManager ChatRoomManager
    {
        get
        {
            return _chatRoomManager;
        }

        set
        {
            _chatRoomManager = value;
        }
    }

    public UILoaderManager UiLoaderManager
    {
        get
        {
            return _uiLoaderManager;
        }
    }

    public User UserData
    {
        get
        {
            return this.RegistrationManager.User;
        }
    }

    #endregion

    #region Quack Mono Behaviour / Singleton

    protected AppManager()
    {
    }

    protected override void OnAwake()
    {
        initServices();
        initManagers();        
    }

    protected override void OnStart()
    {
        Broadcast.Add<RegistrationManager.SignInResponse>(OnSignInEvent);

        if (PlayerPrefsHelper.GetString(PlayerPrefsConsts.PP_LOGIN_METHOD) != PlayerPrefsConsts.PP_FACEBOOK_LOGIN)
        {
            _uiLoaderManager.SetActive(UILoaderManager.eUIType.Registration);
        }
    }
   
    protected override void OnDestroyObject()
    {
        Broadcast.Remove<RegistrationManager.SignInResponse>(OnSignInEvent);
    }

    #endregion

    #region Private Methods

    private void initServices()
    {
        DatabaseService.InitializeFirebase();
    }

    private void initManagers()
    {        
        _registrationManager = instantiateManager<RegistrationManager>(_registrationManagerPFB);
        _chatLobbyManager = instantiateManager<ChatLobbyManager>(_chatLobbyManagerPFB);
        _popupManager = instantiateManager<PopupManager>(_popupManagerPFB);
        _sceneSelectionManager = instantiateManager<SceneSelectionManager>(_sceneSelectionManagerPFB);
        _characterSelectionManager = instantiateManager<SceneCharacterSelectionManager>(_characterSelectionManagerPFB);
        _chatRoomManager = instantiateManager<ChatRoomManager>(_chatRoomManagerPFB);
    }

    private T instantiateManager<T>(T manager) where T : QuackMonoBehaviour
    {
        var go = Instantiate(manager);
        go.transform.SetParent(_managersContainer);
        go.name = manager.name;

        return go;
    }

    #endregion

    #region Event Methods

    // TODO: move to error handling manager
    private void OnSignInEvent(object args)
    {
        var data = (RegistrationManager.SignInResponse)args;

        if (!data.IsSuccessful)
        {
            PopupManager.ShowConfirmPopup(data.message);
            return;
        }
    }

    #endregion
}
