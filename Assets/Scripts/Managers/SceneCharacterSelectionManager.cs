﻿using Assets.Scripts.Data;
using System.Collections.Generic;
using UnityEngine;

public class SceneCharacterSelectionManager : QuackMonoBehaviour
{
    #region Consts

    public const string CHAT_ROOM_FORAMT = "{0}_{1}_{2}";

    #endregion // Consts

    #region Events

    public class ChatCharacterSelectionReady
    {
        public ChatData ChatData;
    }

    #endregion

    #region Private Members

    private SceneData _selectedSceneData;
    private ChatData _currentChatData;
    private Dictionary<string, CharacterData> _sceneCharacters;
    private Dictionary<string, CharacterData> _selectedSceneCharacters;

    #endregion // Private Members

    #region Props

    public Dictionary<string, CharacterData> SelectedSceneCharacters
    {
        get
        {
            return _selectedSceneCharacters;
        }

        set
        {
            _selectedSceneCharacters = value;
        }
    }

    #endregion // Props

    #region Quack Mono Behaviour

    protected override void OnAwake()
    {
        _sceneCharacters = new Dictionary<string, CharacterData>();
        _selectedSceneCharacters = new Dictionary<string, CharacterData>();

        Broadcast.Add<SceneSelectionManager.SceneSelected>(OnSceneSelectedEvent);
        Broadcast.Add<DatabaseService.SceneCharactersDataResponse>(OnSceneCharactersDataResponse);
        Broadcast.Add<DatabaseService.SetChatCharacterDataResponse>(OnChatCharacterDataResponse);
    }

    protected override void OnStart()
    {
        DatabaseService.GetSceneCharacters();
    }

    protected override void OnDestroyObject()
    {
        Broadcast.Remove<SceneSelectionManager.SceneSelected>(OnSceneSelectedEvent);
        Broadcast.Remove<DatabaseService.SceneCharactersDataResponse>(OnSceneCharactersDataResponse);
        Broadcast.Remove<DatabaseService.SetChatCharacterDataResponse>(OnChatCharacterDataResponse);

    }

    #endregion // Quack Mono Behaviour

    #region Public Methods

    public ChatData GetNewChatRoomData()
    {
        var userData = Client.UserData;
        var creatorName = userData.FirstName + "_" + userData.LastName;
        var sceneName = _selectedSceneData.name;
        var chatCount = userData.ChatCount;
        var chatName = string.Format(CHAT_ROOM_FORAMT, creatorName, sceneName, chatCount + 1);

        _currentChatData = new ChatData
        {
            ChatName = chatName,
            Creator = creatorName,
            SceneName = sceneName,
            UserId = userData.Id
        };

        DatabaseService.CreateChatRoom(_currentChatData);

        return _currentChatData;
    }

    public void SaveUserCharacterToChat(CharacterData characterData)
    {
        var chatCharacterData = new ChatCharacterData();
        chatCharacterData.UserId = Client.UserData.Id;
        chatCharacterData.CharacterId = characterData.name;
        chatCharacterData.Position = new Vector2(10f, 15f);
        chatCharacterData.Data = characterData;

        DatabaseService.SetUserChatCharacter(_currentChatData.Uuid, chatCharacterData);
    }    

    #endregion

    #region Event Handlers

    private void OnSceneSelectedEvent(object args)
    {
        var data = (SceneSelectionManager.SceneSelected)args;

        _selectedSceneData = data.SceneData;
        _selectedSceneCharacters.Clear();

        foreach (var kvp in _sceneCharacters)
        {
            var characterName = kvp.Key;
            var characterData = kvp.Value;

            if (!characterData.scenes.ContainsKey("scene_" + _selectedSceneData.id))
            {
                continue;
            }

            _selectedSceneCharacters.Add(characterName, characterData);
        }
    }

    private void OnSceneCharactersDataResponse(object args)
    {
        var data = (DatabaseService.SceneCharactersDataResponse)args;

        _sceneCharacters.Clear();

        foreach (var kvp in data.Data)
        {
            var characterName = kvp.Key;
            var characterData = kvp.Value;

            _sceneCharacters.Add(characterName, characterData);
        }
    }
    
    private void OnChatCharacterDataResponse(object args)
    {
        var data = (DatabaseService.SetChatCharacterDataResponse)args;

        if (!data.IsSuccessful)
        {
            return;
        }

        Broadcast.Trigger(new ChatCharacterSelectionReady() { ChatData = _currentChatData });
    }
    #endregion // Event Handlers
}
