﻿using Assets.Scripts.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UILoaderManager : QuackMonoBehaviour
{
    #region Inspector

    [SerializeField]
    private Transform _uiCanvas;

    [SerializeField]
    List<UIPrefabComponent> _uiPrefabs;

    #endregion

    #region Private Fields

    Dictionary<eUIType, IViewController> _cachedUI;

    #endregion

    #region Quack Mono Behaviour

    protected override void OnAwake()
    {
        _cachedUI = new Dictionary<eUIType, IViewController>();

        Broadcast.Add<RegistrationManager.SignInResponse>(OnSignInRequestEvent);
        Broadcast.Add<ChatLobbyManager.NewChatRoomRequest>(OnNewChatRoomRequestEvent);
        Broadcast.Add<SceneSelectionManager.SceneSelected>(OnSceneSelectedEvent);
        Broadcast.Add<ChatRoomManager.ChatRoomReady>(OnChatRoomReady);
        Broadcast.Add<ChatRoomManager.GoToLobbyRequest>(OnGoToLobbyRequest);
    }

    protected override void OnDestroyObject()
    {
        Broadcast.Remove<RegistrationManager.SignInResponse>(OnSignInRequestEvent);
        Broadcast.Remove<ChatLobbyManager.NewChatRoomRequest>(OnNewChatRoomRequestEvent);
        Broadcast.Remove<SceneSelectionManager.SceneSelected>(OnSceneSelectedEvent);
        Broadcast.Remove<ChatRoomManager.ChatRoomReady>(OnChatRoomReady);
        Broadcast.Remove<ChatRoomManager.GoToLobbyRequest>(OnGoToLobbyRequest);
    }

    #endregion //  Quack Mono Behaviour

    #region Public Methods

    public void SetActive(eUIType uiType, bool state = true)
    {
        foreach (var uiItem in _cachedUI.Where(i=>i.Key != uiType))
        {
            uiItem.Value.SetActive(false);
        }

        if (!_cachedUI.ContainsKey(uiType))
        {
            var prefabToInstantiate = _uiPrefabs.FirstOrDefault(i => i.UiType.Equals(uiType));

            if (prefabToInstantiate == null)
            {
                throw new MissingReferenceException(String.Format("Couldn't find ui prefabe of type [{0}]", uiType));
            }

            var instantiatedGameObject = Instantiate(prefabToInstantiate.UiPrefab);
            _cachedUI.Add(uiType, instantiatedGameObject.GetComponent<BaseViewController>());
            _cachedUI[uiType].Initialize(_uiCanvas);
        }

        _cachedUI[uiType].SetActive(state);
    }

    #endregion // Public Methods

    #region Event Handlers

    private void OnSignInRequestEvent(object args)
    {
        var data = (RegistrationManager.SignInResponse)args;

        if (!data.IsSuccessful)
        {
            return;
        }

        SetActive(eUIType.ChatLobby);
    }

    private void OnNewChatRoomRequestEvent(object args)
    {
        SetActive(eUIType.SceneSelection);
    }

    private void OnSceneSelectedEvent(object args)
    {
        SetActive(eUIType.CharacterSelection);
    }

    private void OnChatRoomReady(object args)
    {
        SetActive(eUIType.ChatRoom);
    }

    private void OnGoToLobbyRequest(object args)
    {
        SetActive(eUIType.ChatLobby);
    }

    #endregion

    [Serializable]
    public class UIPrefabComponent
    {
        [SerializeField]
        private GameObject _uiPrefab;

        [SerializeField]
        private eUIType _uiType;

        public GameObject UiPrefab
        {
            get
            {
                return _uiPrefab;
            }

            set
            {
                _uiPrefab = value;
            }
        }

        public eUIType UiType
        {
            get
            {
                return _uiType;
            }

            set
            {
                _uiType = value;
            }
        }
    }

    public enum eUIType
    {
        Registration,
        ChatLobby,
        Popup,
        SceneSelection,
        CharacterSelection,
        ChatRoom,
    }
}
