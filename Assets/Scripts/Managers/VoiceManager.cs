﻿using Assets.Scripts.Data;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VoiceManager : QuackMonoBehaviour
{
    [SerializeField] private GameObject _chatterPFB;

    public byte Version = 1;

    protected override void OnAwake()
    {
        base.OnAwake();

        PhotonNetwork.autoJoinLobby = false;

        if (!PhotonNetwork.connected)
        {
            QLogger.Log("OnAwake() was called by Unity. Scene is loaded. Let's connect to the Photon Master Server. Calling: PhotonNetwork.ConnectUsingSettings();");
            PhotonNetwork.ConnectUsingSettings(Version + "." + SceneManagerHelper.ActiveSceneBuildIndex);
        }

        Broadcast.Add<RegistrationManager.SignInResponse>(handleSignInRequest);
        Broadcast.Add<DatabaseService.ActiveChatResponse>(handleActiveChatRequest);
    }

    protected override void OnDestroyObject()
    {
        base.OnDestroyObject();

        Broadcast.Remove<RegistrationManager.SignInResponse>(handleSignInRequest);
        Broadcast.Remove<DatabaseService.ActiveChatResponse>(handleActiveChatRequest);
    }
   
    public virtual void OnConnectedToMaster()
    {
        QLogger.Log("OnConnectedToMaster() was called by PUN. Now this client is connected and could join a room. Calling: PhotonNetwork.JoinRandomRoom();");
        //PhotonNetwork.JoinRandomRoom();
    }

    #region Private Methods

    private void loadLobbyData()
    {
        var lobbyType = new TypedLobby(Client.UserData.Id, LobbyType.SqlLobby);
        PhotonNetwork.JoinLobby(lobbyType);
    }

    private void OnJoinedLobby()
    {
        QLogger.Log("Joined lobby.");
       // PhotonNetwork.Instantiate(_chatterPFB.name, Vector3.zero, Quaternion.identity, 0);
    }
    
    #endregion

    #region Event Handlers

    private void handleSignInRequest(object args)
    {
        var data = (RegistrationManager.SignInResponse)args;

        if (!data.IsSuccessful)
        {
            return;
        }

        loadLobbyData();
    }

    private void handleActiveChatRequest(object args)
    {
        var data = (DatabaseService.ActiveChatResponse)args;

        if (!data.IsSuccessful)
        {
            return;
        }

        //foreach (var kvp in data.data)
        //{            
        //    PhotonNetwork.CreateRoom(kvp.Value);
        //}
    }

    #endregion
}
