﻿using Assets.Scripts.Data;
using System;
using System.Collections;
using System.Collections.Generic;

public class ChatRoomManager : QuackMonoBehaviour
{
    #region Events

    public class ChatRoomReady
    {
        public List<ChatCharacterData> Data;
    }

    public class GoToLobbyRequest
    {

    }

    #endregion // Events

    #region Private Members

    private List<ChatCharacterData> _chatRoomCharactersData;

    #endregion // Private Members

    #region Props

    public List<ChatCharacterData> ChatRoomCharactersData
    {
        get
        {
            return _chatRoomCharactersData;
        }
    }

    #endregion // Props

    #region Quack Mono Behaviour

    protected override void OnAwake()
    {
        Broadcast.Add<ChatLobbyManager.ChatRoomSelectionRequest>(OnChatRoomSelectionRequestEvent);
        Broadcast.Add<DatabaseService.GetChatRoomDataResponse>(OnGetChatRoomDataResponse);
        Broadcast.Add<SceneCharacterSelectionManager.ChatCharacterSelectionReady>(OnChatCharacterSelectionReady);
    }

    protected override void OnDestroyObject()
    {
        Broadcast.Remove<ChatLobbyManager.ChatRoomSelectionRequest>(OnChatRoomSelectionRequestEvent);
        Broadcast.Remove<DatabaseService.GetChatRoomDataResponse>(OnGetChatRoomDataResponse);
        Broadcast.Remove<SceneCharacterSelectionManager.ChatCharacterSelectionReady>(OnChatCharacterSelectionReady);
    }

    #endregion

    #region Public Methods

    public void GoToLobby()
    {
        Broadcast.Trigger(new GoToLobbyRequest());
    }

    public void SaveRecord(RecordData data)
    {
        DatabaseService.SaveRecord(data);
    }

    #endregion // Public Methods

    #region Private Methods

    private void SetupChatRoom(string chatUuid)
    {
        // Get chat room data;
        DatabaseService.GetChatRoomData(chatUuid);
    }

    #endregion // Private Methods

    #region Event Handlers

    private void OnChatRoomSelectionRequestEvent(object args)
    {
        var data = (ChatLobbyManager.ChatRoomSelectionRequest)args;

        SetupChatRoom(data.UUID);
    }

    private void OnGetChatRoomDataResponse(object args)
    {
        var data = (DatabaseService.GetChatRoomDataResponse)args;

        if (!data.IsSuccessful)
        {
            return;
        }

        _chatRoomCharactersData = data.Data;

        Broadcast.Trigger(new ChatRoomReady() { Data = _chatRoomCharactersData });
    }

    private void OnChatCharacterSelectionReady(object args)
    {
        var data = (SceneCharacterSelectionManager.ChatCharacterSelectionReady)args;

        SetupChatRoom(data.ChatData.Uuid);
    }

    #endregion // Event Handlers
}
