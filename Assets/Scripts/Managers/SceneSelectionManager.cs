﻿using Assets.Scripts.Data;
using System.Collections.Generic;
using Assets.Scripts.Utils;

public class SceneSelectionManager : QuackMonoBehaviour
{
    #region Events

    public class SceneSelected
    {
        public SceneData SceneData;
    }

    #endregion // Events

    #region Private Members

    private Dictionary<string, SceneData> _availableScenes;

    #endregion // Private Members

    #region Props

    public Dictionary<string, SceneData> AvailableScenes
    {
        get
        {
            return _availableScenes;
        }
    }

    #endregion // Props

    #region Quack Mono Behaviour

    protected override void OnAwake()
    {
        _availableScenes = new Dictionary<string, SceneData>();

        Broadcast.Add<DatabaseService.ScenesDataResponse>(OnScenesDataResponse);
    }

    protected override void OnEnableObject()
    {
        DatabaseService.GetAvailableScenes();
    }

    protected override void OnDisableObject()
    {
        _availableScenes.Clear();
    }

    protected override void OnDestroyObject()
    {
        Broadcast.Remove<DatabaseService.ScenesDataResponse>(OnScenesDataResponse);
    }

    #endregion // Quack Mono Behaviour

    #region Public Methods

    public void OnSceneClick(SceneData data)
    {
        QLogger.Log("Scene " + data.name + " was selected");
        Broadcast.Trigger(new SceneSelected() {SceneData = data });
    }

    #endregion // Public Methods

    #region Event Handlers

    private void OnScenesDataResponse(object args)
    {
        var data = (DatabaseService.ScenesDataResponse)args;

        if (!data.IsSuccessful)
        {
            return;
        }

        _availableScenes = data.Data;
    }

    #endregion // Event Handlers
}
