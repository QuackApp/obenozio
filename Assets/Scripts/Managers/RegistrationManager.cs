﻿using Assets.Scripts.Data;
using Assets.Scripts.Utils;
using Facebook.Unity;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Collections.Generic;
using UnityEngine;

public class RegistrationManager : QuackMonoBehaviour
{
    #region Consts

    private readonly List<string> FAECBOOK_READ_PERMISSIONS = new List<string>() { "public_profile", "email", "user_friends" };
    private readonly string FACEBOOK_API_INFO_QUERY = "/me?fields=first_name,last_name,email";

    #endregion

    #region Events

    public class SignInResponse
    {
        public eRegistrationMethodType MethodType;
        public bool IsSuccessful;
        public eRegistrationResultType ResultType;
        public string message;
    }

    public class SignOutRequest
    {

    }

    #endregion

    #region Inspector

    [SerializeField]
    private GameObject _registrationUI;
    private string _authcode;
    private string _token;
    private string _log;
    private User _user;

    public string Token
    {
        get
        {
            return _token;
        }

        set
        {
            _token = value;
        }
    }

    public string _email { get; private set; }

    public User User
    {
        get
        {
            return _user;
        }
    }

    #endregion

    #region Private Fields

    //private GameObject _registrationGameObject;

    #endregion

    #region Props

    //public GameObject RegistrationGameObject
    //{
    //    get
    //    {
    //        return _registrationGameObject;
    //    }

    //    set
    //    {
    //        _registrationGameObject = value;
    //    }
    //}

    public bool IsLoggedIn
    {
        get
        {
            return FB.IsLoggedIn;
        }
    }
    #endregion

    #region Quack Mono Behaviour

    protected override void OnAwake()
    {
        // Initialize Facebook
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            InitCallback();
        }

        Broadcast.Add<DatabaseService.UserDataResponse>(OnUserDataResponse);
    }

    protected override void OnStart()
    {
        ActivateServices();
    }

    protected override void OnDestroyObject()
    {
        Broadcast.Remove<DatabaseService.UserDataResponse>(OnUserDataResponse);
    }

    #endregion // Quack Mono Behaviour

    #region Public Methods   

    public void SignIn(eRegistrationMethodType registrationMethodType)
    {
        switch (registrationMethodType)
        {
            case eRegistrationMethodType.Facebook:
                {
                    SignInFacebook();
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    public void SignOut(eRegistrationMethodType registrationMethodType)
    {
        switch (registrationMethodType)
        {
            case eRegistrationMethodType.Facebook:
                {
                    SignOutFacebook();
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    #endregion // Public Methods

    #region Private Methods

    #region Faecbook

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();

            if (PlayerPrefsHelper.GetString(PlayerPrefsConsts.PP_LOGIN_METHOD) == PlayerPrefsConsts.PP_FACEBOOK_LOGIN)
            {
                SignInFacebook();
            }
            else
            {
                // show registration options
            }
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    private void SignInFacebook()
    {
        FB.LogInWithReadPermissions(FAECBOOK_READ_PERMISSIONS, AuthCallback);
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            PlayerPrefsHelper.SetString(PlayerPrefsConsts.PP_LOGIN_METHOD, PlayerPrefsConsts.PP_FACEBOOK_LOGIN);

            // If exist in DB get the user data
            var aToken = AccessToken.CurrentAccessToken;
            DatabaseService.GetUserDataById(aToken.UserId);
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    private void CreateNewUserData(IGraphResult result)
    {
        var userData = JSONSerialization<User>.CreateFromJSON(result.RawResult);

        Client.UserData = new User();
        Client.UserData = userData;
        Client.UserData.Token = AccessToken.CurrentAccessToken.TokenString;

        DatabaseService.CreateNewUser(Client.UserData);
    }

    #endregion

    private void ActivateServices()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
         .RequestEmail()
         .RequestServerAuthCode(false)
         .RequestIdToken()
         .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
    }
    
    private void SignOutFacebook()
    {
        FB.LogOut();
    }

    #endregion

    #region Event Handlers

    private void OnUserDataResponse(object args)
    {
        var data = (DatabaseService.UserDataResponse)args;

        if (data.UserData == null)
        {
            // Create new user
            FB.API(FACEBOOK_API_INFO_QUERY, HttpMethod.GET, CreateNewUserData);

            return;
        }

        Client.UserData = new User();
        Client.UserData = data.UserData;

        Broadcast.Trigger(new SignInResponse()
        {
            MethodType = eRegistrationMethodType.Facebook,
            IsSuccessful = data.IsSuccessful,
            ResultType = eRegistrationResultType.LogInSuccess
        });
    }

    #endregion // Event Handlers
}

public enum eRegistrationMethodType
{
    Google,
    Facebook,
}

public enum eRegistrationResultType
{
    AlreadyLoggedIn,
    LogInSuccess,
    LogInFail,
    LogOutSuccess,
    LogOutInFail,
    UnkownService,
}

