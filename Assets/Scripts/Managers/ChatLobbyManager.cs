﻿using Assets.Scripts.Utils;
using System.Collections.Generic;

public class ChatLobbyManager : QuackMonoBehaviour
{
    #region Events

    public class NewChatRoomRequest
    {

    }

    public class ChatRoomSelectionRequest
    {
        public string UUID;
    }

    public class ActiveChatRequest
    {
        public Dictionary<string, string> Data;
    }

    public class InviteChatRequest
    {
        public Dictionary<string, string> Data;
    }

    #endregion

    #region Quack Mono Behaviour

    #endregion //Quack Mono Behaviour

    #region Public Methods

    /// <summary>
    /// Invokes from Unity UI
    /// </summary>
    public void OnNewChatRoomClick()
    {
        QLogger.Log("New chat clicked.");
        Broadcast.Trigger(new NewChatRoomRequest());
    }

    /// <summary>
    /// Invokes from Unity UI
    /// </summary>
    public void OnChatRoomClick(string chatUuid)
    {
        QLogger.Log("Room clicked. UUID = " + chatUuid);
        Broadcast.Trigger(new ChatRoomSelectionRequest() { UUID = chatUuid });
    }

    #endregion // Public Methods
}
