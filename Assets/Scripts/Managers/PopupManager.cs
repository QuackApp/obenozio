﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

public class PopupManager : QuackMonoBehaviour
{
    public event Action OnConfirmButtonClickEvent;

    [SerializeField]
    private PopupConfirm_VC _popupConfirm;

    public void ShowConfirmPopup(string message)
    {
        _popupConfirm.Button.onClick.AddListener(() => ClosePopup());
        _popupConfirm.Text.text = message;

        _popupConfirm.gameObject.SetActive(true);
    }

    private void ClosePopup()
    {
        QLogger.Log("Close popup Clicked!");
        _popupConfirm.gameObject.SetActive(false);

    }
}
