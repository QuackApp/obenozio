﻿using Assets.Scripts.Data;
using Assets.Scripts.Utils;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections.Generic;
using UnityEngine;

public class DatabaseService : BaseSingleton<DatabaseService>
{
    #region Consts

    private const string DB_NODE_SCENES = "scenes";
    private const string DB_NODE_MEMBERS = "members";
    private const string DB_NODE_USERS = "users";
    private const string DB_NODE_CHATS = "chats";
    private const string DB_NODE_CHARACTERS = "characters";
    private const string DB_NODE_CHAT_CHARACTERS = "chatCharacters";
    private const string DB_NODE_RECORDS = "records";

    #endregion

    #region Events

    //============ USER ==============
    public class UserDataResponse
    {
        public bool IsSuccessful;
        public User UserData;
    }

    //============ CHATS ==============
    public class ActiveChatResponse
    {
        public bool IsSuccessful;
        public Dictionary<string, string> Data;
    }

    public class InviteChatResponse
    {
        public bool IsSuccessful;
        public Dictionary<string, string> Data;
    }

    public class SetChatCharacterDataResponse
    {
        public bool IsSuccessful;
    }

    public class GetChatRoomDataResponse
    {
        public bool IsSuccessful;
        public List<ChatCharacterData> Data;
    }

    //============ SCENES ==============
    public class ScenesDataResponse
    {
        public bool IsSuccessful;
        public Dictionary<string, SceneData> Data;
    }

    //============ CHARACTERS ==============
    public class SceneCharactersDataResponse
    {
        public bool IsSuccessful;
        public Dictionary<string, CharacterData> Data;
    }

    #endregion

    #region Members
    
    private DatabaseReference _databaseRef;
    private Dictionary<string, List<ChatCharacterData>> _chatCharacterDataDict;

    #endregion // Members

    #region Public Methods / Singleton

    protected DatabaseService() { }

    public void InitializeFirebase()
    {
        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://quack-53461818.firebaseio.com/");

        // Get the root reference location of the database.
        _databaseRef = FirebaseDatabase.DefaultInstance.RootReference;

        _chatCharacterDataDict = new Dictionary<string, List<ChatCharacterData>>();

        _databaseRef.Child(DB_NODE_CHAT_CHARACTERS).ChildAdded += ChatCharacters_ChildAdded;
    }

    #endregion // Public Methods

    #region API Methods  

    #region Scenes

    public void GetAvailableScenes()
    {
        _databaseRef.Child(DB_NODE_SCENES).OrderByKey().GetValueAsync().ContinueWith(task =>
        {
            Dictionary<string, SceneData> data = null;

            if (task.IsCompleted)
            {
                data = JSONSerialization<SceneData>.CreateDictionaryFromJSON(task.Result.GetRawJsonValue());
            }

            Broadcast.Trigger(new ScenesDataResponse() { IsSuccessful = task.IsCompleted, Data = data });
        });
    }

    public void GetSceneCharacters()
    {
        _databaseRef.Child(DB_NODE_CHARACTERS).GetValueAsync().ContinueWith(task =>
        {
            Dictionary<string, CharacterData> data = null;

            if (task.IsCompleted)
            {
                data = JSONSerialization<CharacterData>.CreateDictionaryFromJSON(task.Result.GetRawJsonValue());
            }

            Broadcast.Trigger(new SceneCharactersDataResponse(){IsSuccessful = task.IsCompleted, Data = data });
        });
    }

    #endregion // Scenes

    #region Chats    

    public void CreateChatRoom(ChatData data)
    {
        data.Uuid = Guid.NewGuid().ToString();
        data.CreatedUTC = DateTime.UtcNow.ToString();

        string json = JsonUtility.ToJson(data);

        _databaseRef.Child(DB_NODE_CHATS).Child(data.Uuid).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                // Update chat count of the user
                UpdateUserChatData(data);

                // Update chat room members
                AddMemberToChatRoom(data);
            }
            else if (task.IsFaulted)
            {
            }
        });
    }

    public void GetUserActiveChats(string userId)
    {
        _databaseRef.Child(DB_NODE_USERS).Child(userId).Child("ActiveChats").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                var data = JSONSerialization<Dictionary<string, string>>.CreateFromJSON(task.Result.GetRawJsonValue());
                Broadcast.Trigger(new ActiveChatResponse() { IsSuccessful = true, Data = data });
            }
            else if (task.IsFaulted)
            {
            }
        });
    }

    public void GetUserInviteChats(string userId)
    {
        _databaseRef.Child(DB_NODE_USERS).Child(userId).Child("InviteChats").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                var data = JSONSerialization<Dictionary<string, string>>.CreateFromJSON(task.Result.GetRawJsonValue());
                Broadcast.Trigger(new InviteChatResponse() { IsSuccessful = true, Data = data });
            }
            else if (task.IsFaulted)
            {
            }
        });
    }

    public void SetUserChatCharacter(string chatId, ChatCharacterData chatCharacterData)
    {
        //var characterChatInstanceId = Guid.NewGuid().ToString();
        string json = JsonUtility.ToJson(chatCharacterData);

        _databaseRef.Child(DB_NODE_CHAT_CHARACTERS).Child(chatId).Child(chatCharacterData.UserId).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            Broadcast.Trigger(new SetChatCharacterDataResponse() { IsSuccessful = task.IsCompleted });
        });
    }

    public void GetChatRoomData(string chatUuid)
    {
        _databaseRef.Child(DB_NODE_CHAT_CHARACTERS).Child(chatUuid).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                var data = JSONSerialization<ChatCharacterData>.CreateFromJSON(task.Result.GetRawJsonValue());

                Broadcast.Trigger(new GetChatRoomDataResponse() { IsSuccessful = true, Data = _chatCharacterDataDict[chatUuid]});
            }
            else if (task.IsFaulted)
            {
            }
        });
    }

    #endregion // Chats

    public void SaveRecord(RecordData data)
    {
        //data.CreatedUTC = DateTime.UtcNow.ToShortDateString();

       // var json = JSONSerialization<RecordData>.CreateFromObject(data);

        //_databaseRef.Child(DB_NODE_RECORDS).Child(data.ChatRoomId).Push().SetRawJsonValueAsync(json).ContinueWith(task =>
        //{

        //});
    }

    #region User Data

    public void GetUserDataById(string id)
    {
        _databaseRef.Child(DB_NODE_USERS).Child(id).GetValueAsync().ContinueWith(task =>
        {
            User userdata = null;
            if (task.IsCompleted)
            {
                if (task.Result.Exists)
                {
                    userdata = JSONSerialization<User>.CreateFromJSON(task.Result.GetRawJsonValue());
                }
            }
            else if (task.IsFaulted)
            {
            }

            Broadcast.Trigger(new UserDataResponse() { IsSuccessful = task.IsCompleted, UserData = userdata });
        });
    }

    public void CreateNewUser(User user)
    {
        string json = JSONSerialization<User>.CreateFromObject(user);

        _databaseRef.Child(DB_NODE_USERS).Child(user.Id).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            GetUserDataById(user.Id);
        });
    }

    #endregion

    #endregion

    #region Private Methods

    private void ChatCharacters_ChildAdded(object sender, ChildChangedEventArgs e)
    {
        var parent = e.Snapshot.Key;

        if (!_chatCharacterDataDict.ContainsKey(parent))
        {
            _chatCharacterDataDict.Add(parent, new List<ChatCharacterData>());
        }

        var data = JSONSerialization<ChatCharacterData>.CreateDictionaryFromJSON(e.Snapshot.GetRawJsonValue());

        foreach (var character in data)
        {
            character.Value.Key = character.Key;
            _chatCharacterDataDict[parent].Add(character.Value);
        }
    }

    private void UpdateUserChatData(ChatData chatData)
    {
        var user = Client.UserData;
        _databaseRef.Child(DB_NODE_USERS).Child(user.Id).Child("ChatCount").SetValueAsync(++user.ChatCount);
        _databaseRef.Child(DB_NODE_USERS).Child(user.Id).Child("ActiveChats").Child(chatData.Uuid).SetValueAsync(chatData.ChatName);

        Client.UserData.ActiveChats.Add(chatData.Uuid, chatData.ChatName);
    }

    private void AddMemberToChatRoom(ChatData chatData)
    {
        var user = AppManager.Instance.RegistrationManager.User;
        _databaseRef.Child(DB_NODE_MEMBERS).Child(chatData.Uuid).Child(user.Id).SetValueAsync(true);
    }

    #endregion
}
