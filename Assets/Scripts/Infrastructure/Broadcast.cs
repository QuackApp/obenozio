﻿using System.Collections.Generic;
using UnityEngine.Events;

public class Broadcast : BaseSingleton<Broadcast>
{
    private class UnityEventObj : UnityEvent<object>
    { }

    private static Dictionary<object, UnityEventObj> _eventDictionary = new Dictionary<object, UnityEventObj>();

    public static void Add<T>(UnityAction<object> listener)
    {
        if (!_eventDictionary.ContainsKey(typeof(T)))
        {
            _eventDictionary.Add(typeof(T), new UnityEventObj());
        }

        _eventDictionary[typeof(T)].AddListener(listener);
    }

    public static void Remove<T>(UnityAction<object> listener)
    {
        if (_eventDictionary.ContainsKey(typeof(T)))
        {
            _eventDictionary[typeof(T)].RemoveListener(listener);
        }
    }

    public static void Trigger<T>(T eventClass)
    {
        if (_eventDictionary.ContainsKey(eventClass.GetType()))
        {
            _eventDictionary[typeof(T)].Invoke(eventClass);
        }
    }
}
