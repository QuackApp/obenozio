﻿using UnityEngine;

public class BaseViewController : QuackMonoBehaviour, IViewController
{
    private Transform _uiParent;

    public void Initialize(Transform parent)
    {
        _uiParent = parent;
        transform.SetParent(_uiParent, false);

        OnInitialize();
    }

    public void SetActive(bool state)
    {
        bool changeState = (state && !gameObject.activeInHierarchy) || (!state && gameObject.activeInHierarchy);

        if (changeState)
        {
            gameObject.SetActive(state);
        }
    }


    protected virtual void OnInitialize()
    {
    }
}
