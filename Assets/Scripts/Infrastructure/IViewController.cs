﻿using UnityEngine;

public interface IViewController
{
    void Initialize(Transform parent);

    void SetActive(bool state);
}
