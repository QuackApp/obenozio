﻿using Assets.Scripts.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ChatLobby_VC : BaseViewController
{
    #region Members

    [SerializeField]
    private ChatSlot_VC _chatSlotPFB;

    [SerializeField]
    private RectTransform _activeChatsContainer;

    [SerializeField]
    private RectTransform _inviteChatsContainer;

    private Dictionary<string, ChatSlot_VC> _userChats;
    private Dictionary<string, ChatSlot_VC> _userInvites;

    #endregion // Members

    #region Quack Mono Behaviour

    protected override void OnAwake()
    {
        _userChats = new Dictionary<string, ChatSlot_VC>();
        _userInvites = new Dictionary<string, ChatSlot_VC>();
    }

    protected override void OnEnableObject()
    {
        SetUserActiveChats();
        SetUserInvites();
    }

    protected override void OnDisableObject()
    {
        foreach (var item in _userChats)
        {
            item.Value.DestorySelf();
        }

        foreach (var item in _userInvites)
        {
            item.Value.DestorySelf();
        }

        _userChats.Clear();
        _userInvites.Clear();
    }

    #endregion // Quack Mono Behaviour

    #region Public Methods

    public void OnNewChatClick()
    {      
        AppManager.ChatLobbyManager.OnNewChatRoomClick();
    }

    #endregion // Public Methods

    #region Private Methods

    private void SetUserActiveChats()
    {
        SetUserChats(_activeChatsContainer, _userChats, Client.UserData.ActiveChats);
    }

    private void SetUserInvites()
    {
        SetUserChats(_inviteChatsContainer, _userInvites, Client.UserData.Invites);
    }

    private void SetUserChats(RectTransform parent, Dictionary<string, ChatSlot_VC> referenceHolder, Dictionary<string, string> chats)
    {
        foreach (var kvp in chats)
        {
            var chatId = kvp.Key;
            var chatName = kvp.Value;
            var instance = Instantiate(_chatSlotPFB);

            instance.gameObject.name = chatName;
            instance.transform.SetParent(parent, false);
            instance.Initialize(chatId, chatName);

            referenceHolder.Add(chatId, instance);
        }
    }

    #endregion // Private Methods
}