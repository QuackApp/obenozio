﻿using Assets.Scripts.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneSlot_VC : QuackMonoBehaviour
{
    [SerializeField]
    private GameObject _slot;

    [SerializeField]
    private Button _clickable;

    [SerializeField]
    private Text _sceneName;

    private SceneData _sceneData;

    public Button Clickable
    {
        get
        {
            return _clickable;
        }
    }

    public Text SceneName
    {
        get
        {
            return _sceneName;
        }
    }
    public SceneData SceneData
    {
        get
        {
            return _sceneData;
        }
    }

    public void Initialize(SceneData sceneData)
    {
        _sceneData = sceneData;
        _sceneName.text = sceneData.name;
    }

    public void OnClick()
    {
        AppManager.SceneSelectionManager.OnSceneClick(_sceneData);
    }
}
