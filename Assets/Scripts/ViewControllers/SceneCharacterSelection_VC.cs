﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Data;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Utils;

public class SceneCharacterSelection_VC : BaseViewController
{    
    [Header("SceneProperties")]
    [SerializeField]
    private Text _sceneTitle;
    [SerializeField]
    private Text _chatRoomTitle;
    [SerializeField]
    private Image _sceneImage;

    [SerializeField]
    private CharacterSlot_VC _characterSlotPFB;

    [SerializeField]
    private RectTransform _container;


    private Dictionary<string, CharacterSlot_VC> _characterSlots;
    private Dictionary<string, CharacterData> _characters;

    protected override void OnAwake()
    {
        _characters = new Dictionary<string, CharacterData>();
        _characterSlots = new Dictionary<string, CharacterSlot_VC>();
    }

    protected override void OnEnableObject()
    {
        SetSceneVariables();
        SetSceneCharacters();
    }

    protected override void OnDisableObject()
    {
        foreach (var item in _characterSlots)
        {
            item.Value.DestorySelf();
        }

        _characterSlots.Clear();
        _characters.Clear();
    }

    private void SetSceneVariables()
    {
       var chatData = AppManager.CharacterSelectionManager.GetNewChatRoomData();

        _sceneTitle.text = chatData.SceneName;
        _chatRoomTitle.text = chatData.ChatName;
    }

    private void SetSceneCharacters()
    {
        _characterSlots.Clear();

        foreach (var character in AppManager.CharacterSelectionManager.SelectedSceneCharacters)
        {
            var instance = Instantiate(_characterSlotPFB);
            instance.gameObject.name = character.Key;
            instance.transform.SetParent(_container, false);
            instance.Initialize(character.Value);

            _characterSlots.Add(character.Key, instance);
        }
    }
}
