﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Utils;
using Assets.Scripts.Data;

public class SceneSelection_VC : BaseViewController
{
    [SerializeField]
    private SceneSlot_VC _sceneSlotPFB;

    [SerializeField]
    private RectTransform _scenesContainer;

    private Dictionary<string, SceneData> _scenes;
    private Dictionary<string, SceneSlot_VC> _availableScenes;

    #region Quack Mono Behaviour

    protected override void OnAwake()
    {
        _availableScenes = new Dictionary<string, SceneSlot_VC>();
    }

    protected override void OnEnableObject()
    {
        // Get available scenes from database
        SetAvailableScenes();
    }

    protected override void OnDisableObject()
    {
        foreach (var item in _availableScenes)
        {
            item.Value.DestorySelf();
        }

        _availableScenes.Clear();
    }

    protected override void OnDestroyObject()
    {
        foreach (var item in _availableScenes)
        {
            item.Value.DestorySelf();
        }

        _availableScenes.Clear();
    }

    #endregion // Quack Mono Behaviour

    #region Private Methods

    private void SetAvailableScenes()
    {
        foreach (var scene in AppManager.SceneSelectionManager.AvailableScenes)
        {
            if (!_availableScenes.ContainsKey(scene.Key))
            { 
                var sceneSlotInstance = Instantiate(_sceneSlotPFB);
                sceneSlotInstance.gameObject.name = scene.Key;
                sceneSlotInstance.transform.SetParent(_scenesContainer, false);
                _availableScenes.Add(scene.Key, sceneSlotInstance);
            }

            _availableScenes[scene.Key].Initialize(scene.Value);
        }
    }

    #endregion // Private Methods
}
