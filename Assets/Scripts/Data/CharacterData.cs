﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Data
{
    [Serializable]
    public class CharacterData
    {
        public string name;

        public bool IsFullScreen;

        public string animationController;

        public Dictionary<string, bool> scenes;

        [JsonProperty("animations")]
        public Dictionary<string, bool> animations;
    }
}
