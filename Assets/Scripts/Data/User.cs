﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Assets.Scripts.Data
{
    public class User
    {
        [JsonProperty("first_name")]  
        public string FirstName;

        [JsonProperty("last_name")]
        public string LastName;

        public string Username;

        [JsonProperty("email")]
        public string Email;

        public string Token;

        [JsonProperty("id")]
        public string Id;

        public int ChatCount;

        public Dictionary<string, string> ActiveChats;

        public Dictionary<string, string> Invites;

        public User()
        {
            ChatCount = 0;
            ActiveChats = new Dictionary<string, string>();
            Invites = new Dictionary<string, string>();
        }
    }
}
