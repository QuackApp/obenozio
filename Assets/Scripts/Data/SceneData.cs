﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Data
{
    [Serializable]
    public class SceneData
    {
        public int id;
        public string image;
        public bool isActive;
        public string name;
        public Dictionary<string, bool> characters;
    }
}
