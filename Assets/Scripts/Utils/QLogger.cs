﻿using System;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class QLogger
    {
        private static bool printLogs = true;
        private static bool printWarnings = true;
        private static bool printErrors = true;
        private static bool printExceptions = true;

        public static void Log(string message)
        {
            if (!ShouldPrint(printLogs))
            {
                return;
            }

            Debug.Log(message);
        }

        public static void LogWarning(string message)
        {
            if (!ShouldPrint(printWarnings))
            {
                return;
            }

            Debug.LogWarning(message);
        }

        public static void LogError(string message)
        {
            if (!ShouldPrint(printErrors))
            {
                return;
            }

            Debug.LogError(message);
        }

        public static void LogException(Exception exception)
        {
            if (!ShouldPrint(printExceptions))
            {
                return;
            }

            Debug.LogException(exception);
        }

        private static bool ShouldPrint(bool printLevel)
        {
            return printLevel;
        }
    }
}
