﻿using UnityEngine;

public class Rotate : QuackMonoBehaviour
{
    private float _angle = 4.0f;

    protected override void OnUpdate()
    {
        transform.Rotate(Vector3.back, _angle);
    }
}
